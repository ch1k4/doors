from django.conf.urls import patterns, include, url
from transactions.views import Index

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', Index.as_view(), name='index'),
    url(r'^transactions/api/', include('transactions.RESTurls')),
    url(r'^transactions/', include('transactions.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #standad django login view
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='account-login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='account-logout'),
)
