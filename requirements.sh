#!/bin/bash          

echo Install Requirements

read -p "Press any key... To Install import Export extension"
sudo pip install django-import-export

read -p "Press any key... To Install Bootstrap 3 Date time picker extension"
sudo pip install django-bootstrap3-datetimepicker

read -p "Press any key... To Install crispy-forms extension"
sudo pip install django-crispy-forms

read -p "Press any key... To Install floppyforms extension"
sudo pip install django-floppyforms

read -p "Press any key... To Sync databases"
python manage.py migrate

read -p "Press any key... To Install South extension"
sudo easy_install South

while true; do
		read -p "Start server? [y/N]" yn
		case $yn in
		[Yy]* ) python manage.py runserver ec2-54-69-118-136.us-west-2.compute.amazonaws.com:8000;;
		[Nn]* ) exit ;;
		* ) echo "Please answer yes or no.";;
		esac
	done