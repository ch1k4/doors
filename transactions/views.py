from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404
from transactions.models import *
from django.core.urlresolvers import reverse
from datetime import datetime, date
import forms
from django.shortcuts import redirect
from django.views.generic import View
from django.db.models import Sum
#new code
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

#Email settings
from django.conf import settings
from django.core.mail import send_mail
from django.views.generic import FormView
from transactions.forms import ContactForm

#Number of records before next page
Paginate = 8


# Create your views here.

class loginmixin(View):
    
    @method_decorator(login_required)
    def dispatch(self,request,*args,**kwargs):
        return super(loginmixin, self).dispatch(request,*args, **kwargs)

class usermixin(View):
        
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = Operator.objects.get(pk=self.request.user.id)
        self.object = form.save()

        return super(usermixin, self).form_valid(form)

class Index(TemplateView, loginmixin):
    
    template_name = "transactions/index.html"

    
    def get_context_data(self,*args, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        period_list = Period.objects.filter(created_by = self.request.user.id)[:5]
        shift_list = Shift.objects.filter(created_by = self.request.user.id)
        expense_list = Expense.objects.filter(created_by = self.request.user.id)[:10]
        total_Earnings = Period.objects.filter(paid=1,created_by = self.request.user.id).aggregate(Sum('period_pay'))
        total_expenses = Expense.objects.filter(created_by = self.request.user.id).aggregate(Sum('item_price'))
       
        for earnings in total_Earnings.values(): 
            for expenses in total_expenses.values():
                if earnings and expenses:
                    net_pay = earnings - expenses
                else:
                    net_pay = 0

        context.update({
            "period_list":period_list,
            "shift_list":shift_list,
            "expense_list":expense_list,
            "total_earnings":total_Earnings,
            "total_expense":total_expenses,
            "net_pay":net_pay,    
        })
        return context

class ContactFormView(FormView, loginmixin):

    form_class = ContactForm
    template_name = "transactions/email_form.html"
    success_url = 'sent/'
    
    def form_valid(self, form):
        message = form.cleaned_data.get('message')
        number = str(form.cleaned_data.get('number'))+'@desksms.appspotmail.com'
            
        send_mail(
            subject="Shifts",
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[number],
        )
        print number
        return super(ContactFormView, self).form_valid(form)
    
    def get_initial(self):
        self.period = get_object_or_404(Period, id = self.kwargs.get("pk",None))
        shift_list = Shift.objects.filter(shift_period = self.period, created_by = self.request.user.id)
        message = 'Shifts for ' +str(self.period)
        
        for shift in shift_list:
            message += '\n' +str(shift.shift_location) +' '+ shift.shift_from_date_and_time.strftime("%A %d, %I:%M%p")+' - '+ shift.shift_to_date_and_time.strftime("%H:%M%p") + ' ('+ str(shift.hours_worked_string())+')'
            
        return { 'message': message, 'number':'447979958231' }
    
class EmailSent(TemplateView, loginmixin):
    template_name = "transactions/email_sent.html"

class ShiftList(ListView, loginmixin):
    model = Shift
    paginate_by = Paginate
    
    def get_queryset(self):
        return Shift.objects.filter(created_by = self.request.user.id)

class DebtorList(ListView, loginmixin):
    model = Debtor
    paginate_by = Paginate
       
    def get_context_data(self,*args, **kwargs):
        context = super(DebtorList, self).get_context_data(**kwargs)
        debt_list = Debt.objects.filter(created_by = self.request.user.id)
        debt_owed = debt_list.aggregate(Sum('loan_amount'))
        debt_paid = Payment.objects.filter(debt__in=debt_list).aggregate(Sum('amount'))
        debt_outstanding = debt_list.filter(fully_paid = False).aggregate(Sum('balance'))
        
        
        context.update({
            "debt_list":debt_list,
            "debt_owed":debt_owed,
            "debt_paid":debt_paid,
            "debt_outstanding":debt_outstanding,
        })
        return context
    
    def get_queryset(self):
        return Debtor.objects.filter(created_by = self.request.user.id)
    
class PeriodList(ListView, loginmixin):
    model = Period
    paginate_by = Paginate
    
    def get_queryset(self):
        return Period.objects.filter(created_by = self.request.user.id)
    
class PeriodDetail(DetailView, loginmixin):
    model = Period
    
class DebtorDebt(DetailView, loginmixin):
    model = Debtor 
    paginate_by = Paginate
    template_name = 'transactions/debtor_debt.html'
       
    def get_context_data(self,*args, **kwargs):
        context = super(DebtorDebt, self).get_context_data(**kwargs)
        debt_list = Debt.objects.filter(created_by = self.request.user.id, debtor_name = self.kwargs['pk'])
        debt_owed = debt_list.aggregate(Sum('loan_amount'))
        debt_paid = Payment.objects.filter(debt__in=debt_list).aggregate(Sum('amount'))
        debt_outstanding = debt_list.filter(fully_paid = False).aggregate(Sum('balance'))
        
        
        context.update({
            "debt_list":debt_list,
            "debt_owed":debt_owed,
            "debt_paid":debt_paid,
            "debt_outstanding":debt_outstanding,
        })
        return context

class ShiftDetail(DetailView, loginmixin):
    model = Shift

class DebtDetail(DetailView, loginmixin):
    model = Debt
       
    def get_context_data(self,*args, **kwargs):
        context = super(DebtDetail, self).get_context_data(**kwargs)
        payment_list = Payment.objects.filter(created_by = self.request.user.id, debt = self.kwargs['pk'])
    
        context.update({
            "payment_list":payment_list, 
        })
        return context
    
class PeriodShifts(ListView, loginmixin):
    template_name = 'transactions/shifts_in_period.html'
    
    def get_queryset(self):
        self.period = get_object_or_404(Period, id = self.kwargs.get("periodpk",None))
        return Shift.objects.filter(shift_period = self.period, created_by = self.request.user.id)

class LocationList(ListView, loginmixin):
    model = Location
    paginate_by = Paginate
    queryset = Location.objects.order_by('location_name')

class OperatorList(ListView, loginmixin):
    model = Operator
    paginate_by = Paginate
    
class PayList(ListView, loginmixin):
    model = Pay
    paginate_by = Paginate
    
    def get_queryset(self):
        return Pay.objects.filter(created_by = self.request.user.id)

class ExpenseList(ListView, loginmixin):
    model = Expense
    paginate_by = Paginate
    
    def get_queryset(self):
        return Expense.objects.filter(created_by = self.request.user.id)

class CreateExpense(usermixin, CreateView, loginmixin):
    model = Expense
    form_class = forms.ExpenseForm
    template_name = 'transactions/create_expense.html'
  
    def get_success_url(self):
        return reverse('expense-list')

class CreateLocation(CreateView, loginmixin):
    model = Location
    template_name = 'transactions/create_location.html'
    
    def get_success_url(self):
        return reverse('location-list')
    
class CreateDebtor(usermixin, CreateView, loginmixin):
    model = Debtor
    template_name = 'transactions/create_debtor.html'
    form_class = forms.DebtorForm
    
    def get_success_url(self):
        return reverse('debtor-list')
    
class CreateDebt(CreateView, loginmixin):
    model = Debt
    template_name = 'transactions/create_debt.html'
    form_class = forms.DebtForm
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = Operator.objects.get(pk=self.request.user.id)
        self.object.debtor_name = Debtor.objects.get(pk=self.kwargs.get('pk'))
        self.object = form.save()
        
        return super(CreateDebt, self).form_valid(form)
           
    def get_context_data(self,*args, **kwargs):
        context = super(CreateDebt, self).get_context_data(**kwargs)
        debtor_id = self.kwargs.get('pk')
    
        context.update({
            "pk":debtor_id,
        })
        return context
    
    def get_initial(self):
        self.debt = Debt()
        content = date.today().strftime("%Y-%m-%d")
         
        return { 'date_loaned': content }
    
    def get_success_url(self):
        return reverse('debtor-debt', args= (self.kwargs.get('pk'),))
    
class CreatePayment(CreateView, loginmixin):
    model = Payment
    template_name = 'transactions/create_payment.html'
    form_class = forms.PaymentForm
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.created_by = Operator.objects.get(pk=self.request.user.id)
        self.object.debt = Debt.objects.get(pk=self.kwargs.get('pk'))
        self.object = form.save()
        
        return super(CreatePayment, self).form_valid(form)
           
    def get_context_data(self,*args, **kwargs):
        context = super(CreatePayment, self).get_context_data(**kwargs)
        debt_id = self.kwargs.get('pk')
    
        context.update({
            "pk":debt_id,
        })
        return context 
    
    def get_initial(self):
        self.debt = Debt()
        content = date.today().strftime("%Y-%m-%d")
         
        return { 'date': content }
    
    def get_success_url(self):
        return reverse('debt-detail', args= (self.kwargs.get('pk'),))

class CreatePay(usermixin, CreateView, loginmixin):
    model = Pay
    template_name = 'transactions/create_pay.html'
    form_class = forms.PayForm
    
    def get_success_url(self):
        return reverse('pay-list')

class CreatePersonnel(CreateView, loginmixin):
    model = Operator
    template_name = 'transactions/create_personnel.html'
    
    def get_success_url(self):
        return reverse('personnel-list')
    
class CreatePeriod(usermixin, CreateView, loginmixin):
    model = Period
    form_class = forms.PeriodForm
    template_name = 'transactions/create_period.html'
    
    def get_success_url(self):
        return reverse('period-shifts', args=(Period.objects.filter(created_by= self.request.user.id).order_by('-id')[0].id,))
    
    def get_initial(self):
        self.period = Period()
        content = 'W/e ' + self.period.week_ending_date().strftime("%d/%m/%y")
         
        return { 'period': content }

class CreateShift(usermixin, CreateView, loginmixin):
    model = Shift
    form_class = forms.ShiftForm
    template_name = 'transactions/create_shift.html'
    
    def get_form_kwargs(self):
        kwargs = super(CreateShift, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse('shift-detail', args=(Shift.objects.filter(created_by= self.request.user.id).order_by('-id')[0].id,))
    
class UpdateLocation(UpdateView, loginmixin):
    model = Location
    template_name = 'transactions/update_location.html'
    
    def get_success_url(self):
        return reverse('location-list')
    
class UpdatePeriod(UpdateView, loginmixin):
    model = Period 
    form_class = forms.PeriodUpdateForm
    template_name = 'transactions/update_period.html'
    
    def get_success_url(self):
        return reverse('period-shifts', args=(self.kwargs['pk'],))
    
class UpdateShift(UpdateView, loginmixin):
    model = Shift
    form_class = forms.ShiftUpdateForm
    template_name = 'transactions/update_shift.html'
    
    def get_success_url(self):
        return reverse('shift-detail', args=(self.kwargs['pk'],))
    
class UpdatePersonnel(UpdateView, loginmixin):
    model = Operator 
    form_class = forms.OperatorUpdateForm
    template_name = 'transactions/update_personnel.html'
    
    def get_success_url(self):
        return reverse('index')
    
class UpdatePay(UpdateView, loginmixin):
    model = Pay
    form_class = forms.PayUpdateForm
    template_name = 'transactions/update_pay.html'
    
    def get_success_url(self):
        return reverse('pay-list')
    
class UpdateExpense(UpdateView, loginmixin):
    model = Expense
    form_class = forms.ExpenseUpdateForm
    template_name = 'transactions/update_expense.html'
    
    def get_success_url(self):
        return reverse('expense-list')
    
    
class UpdateDebtor(UpdateView, loginmixin):
    model = Debtor
    form_class = forms.DebtorUpdateForm
    template_name = 'transactions/update_debtor.html'
    
    def get_success_url(self):
        return reverse('debtor-debt', args=(self.kwargs['pk'],))
    
class UpdateDebt(UpdateView, loginmixin):
    model = Debt
    form_class = forms.DebtUpdateForm
    template_name = 'transactions/update_debt.html'
    
    def get_success_url(self):
        self.debtor = get_object_or_404(Debt, id = self.kwargs['pk'],).debtor_name.id
        return reverse('debtor-debt', args=(self.debtor,))
    
class UpdatePayment(UpdateView, loginmixin):
    model = Payment
    form_class = forms.PaymentUpdateForm
    template_name = 'transactions/update_payment.html'
    
    def get_success_url(self):
        self.debt = get_object_or_404(Payment, id = self.kwargs['pk'],).debt.id
        return reverse('debt-detail', args=(self.debt,))
    
