from django.contrib import admin
from transactions.models import *
from import_export.admin import ImportExportModelAdmin

class ShiftAdmin(ImportExportModelAdmin):
  list_display = ('shift_from_date_and_time','created_by','shift_location','hours_worked_string','shift_pay')
  fields =['shift_period','created_by','shift_from_date_and_time','shift_to_date_and_time','shift_location','shift_pay_rate','shift_comments']
  list_filter = ['shift_period','shift_location','shift_from_date_and_time']
  resource_class = ShiftResource
  pass

class PeriodAdmin(ImportExportModelAdmin):
  list_display = ('period','created_by','shifts_this_period','personal_protection','period_hours','period_pay','paid', 'date_received')
  fields =['period','paid']
  resource_class = PeriodResource
  '''fieldsets =[
    (None, {'fields': ['period']['paid']})
    ,('Shift information', {'fields': ['shifts_this_period']})
  ]'''
  pass
  
class PayAdmin(ImportExportModelAdmin):
  resource_class = PayResource
  list_display =('pay','created_by','pay_rate')
  fields = ['pay','pay_rate']
  pass
  
class LocationAdmin(ImportExportModelAdmin):
  resource_class = LocationResource
  pass
  
class OperatorAdmin(ImportExportModelAdmin):
  resource_class = OperatorResource
  pass
  
class ExpenseAdmin(ImportExportModelAdmin):
  resource_class = ExpenseResource
  list_display =('item_description','created_by')
  pass

  
class DebtorAdmin(ImportExportModelAdmin):
  resource_class = DebtorResource
  pass

  
class DebtAdmin(ImportExportModelAdmin):
  resource_class = DebtResource
  pass

  
class PaymentAdmin(ImportExportModelAdmin):
  resource_class = PaymentResource
  pass

# Register your models here.
admin.site.register(Expense, ExpenseAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Shift, ShiftAdmin)
admin.site.register(Pay, PayAdmin)
admin.site.register(Debtor, DebtorAdmin)
admin.site.register(Debt, DebtAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Period, PeriodAdmin)
admin.site.register(Operator, OperatorAdmin)