
from .models import *
from .RESTserializers import *

from django.http import Http404

from rest_framework.generics import (ListCreateAPIView, RetrieveUpdateDestroyAPIView)
from .RESTpermissions import IsOwnerOrReadOnly

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

class UserFilterMixin(object):

    def get_queryset(self):
        queryset = super(UserFilterMixin, self).get_queryset()
        return queryset.filter(created_by = self.request.user.id)

class PermissionMixin(object):
    permission_classes = (IsAuthenticated,IsOwnerOrReadOnly,)
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    paginate_by = 20

    def perform_create(self, serializer):
        serializer.save(created_by=Operator.objects.get(pk=self.request.user.id)) 

class DebtorMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Debtor.objects.all()
    serializer_class = DebtorSerialiser

class DebtorList(DebtorMixin, ListCreateAPIView):
    pass

class DebtorDetail(DebtorMixin, RetrieveUpdateDestroyAPIView):
    pass

class LocationMixin(PermissionMixin,object):
    queryset = Location.objects.all()
    serializer_class = LocationSerialiser

class LocationList(LocationMixin, ListCreateAPIView):
    pass

class LocationDetail(LocationMixin, RetrieveUpdateDestroyAPIView):
    pass

class OperatorMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Operator.objects.all()
    serializer_class = OperatorSerialiser

class OperatorList(OperatorMixin, ListCreateAPIView):
    pass

class OperatorDetail(OperatorMixin, RetrieveUpdateDestroyAPIView):
    pass

class PayMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Pay.objects.all()
    serializer_class = PaySerialiser

class PayList(PayMixin, ListCreateAPIView):
    pass

class PayDetail(PayMixin, RetrieveUpdateDestroyAPIView):
    pass

class PeriodMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Period.objects.all()
    serializer_class = PeriodSerialiser

class PeriodList(PeriodMixin, ListCreateAPIView):
    pass

class PeriodDetail(PeriodMixin, RetrieveUpdateDestroyAPIView):
    pass

class ShiftMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Shift.objects.all()
    serializer_class = ShiftSerialiser

class ShiftList(ShiftMixin, ListCreateAPIView):
    pass

class ShiftDetail(ShiftMixin, RetrieveUpdateDestroyAPIView):
    pass

class DebtMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Debt.objects.all()
    serializer_class = DebtSerialiser

class DebtList(DebtMixin, ListCreateAPIView):
    pass

class DebtDetail(DebtMixin, RetrieveUpdateDestroyAPIView):
    pass

class PaymentMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerialiser

class PaymentList(PaymentMixin, ListCreateAPIView):
    pass

class PaymentDetail(PaymentMixin, RetrieveUpdateDestroyAPIView):
    pass

class ExpenseMixin(UserFilterMixin, PermissionMixin,object):
    queryset = Expense.objects.all()
    serializer_class = ExpenseSerialiser

class ExpenseList(ExpenseMixin, ListCreateAPIView):
    pass

class ExpenseDetail(ExpenseMixin, RetrieveUpdateDestroyAPIView):
    pass

