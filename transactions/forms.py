from bootstrap3_datetime.widgets import DateTimePicker
from django import forms
from django.contrib.admin import widgets
from transactions.models import *

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
import floppyforms as forms

class ShiftForm(forms.ModelForm):
    shift_comments = forms.CharField(widget=forms.Textarea(attrs={'cols':50,'rows':15}))
    shift_from_date_and_time = forms.DateTimeField(widget=DateTimePicker(options={"format": "yyyy-mm-dd hh:mm","pickTime": True}))
    shift_to_date_and_time = forms.DateTimeField(widget=DateTimePicker(options={"format": "yyyy-mm-dd hh:mm","pickTime": True}))
                
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        # now kwargs doesn't contain 'user', so we can safely pass it to the base class method
        super(ShiftForm, self).__init__(*args, **kwargs)
        self.fields['shift_period'].queryset = Period.objects.filter(created_by=user, paid = False)
        self.fields['shift_pay_rate'].queryset = Pay.objects.filter(created_by=user)
        
    class Meta:
        model = Shift
        exclude = ['shift_pay','shift_hours','created_by']

class PeriodForm(forms.ModelForm):
    class Meta:
        model = Period
        exclude = ['period_hours','personal_protection','period_pay','date_received', 'paid','period_week_ending_date','created_by']

class PeriodUpdateForm(forms.ModelForm):
    class Meta:
        model = Period
        exclude = ['created_by']

class ShiftUpdateForm(forms.ModelForm):
    class Meta:
        model = Shift
        exclude = ['created_by']

class PayUpdateForm(forms.ModelForm):
    class Meta:
        model = Pay
        exclude = ['created_by']

class ExpenseUpdateForm(forms.ModelForm):
    class Meta:
        model = Expense
        exclude = ['created_by']
        
class DebtorUpdateForm(forms.ModelForm):
    class Meta:
        model = Debtor
        exclude = ['created_by']
        
class PaymentUpdateForm(forms.ModelForm):
    class Meta:
        model = Payment
        exclude = ['created_by']
        
class DebtUpdateForm(forms.ModelForm):
    class Meta:
        model = Debt
        exclude = ['created_by','debtor_name','fully_paid','balance','date_cleared']

class OperatorUpdateForm(forms.ModelForm):
    class Meta:
        model = Operator
        exclude = ['operator']

class ContactForm(forms.Form):

    number = forms.IntegerField(required=True)
    message = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Send'))
        super(ContactForm, self).__init__(*args, **kwargs)
        
class ExpenseForm(forms.ModelForm):
    date_bought = forms.DateTimeField(widget=DateTimePicker(options={"format": "yyyy-mm-dd hh:mm","pickTime": True}))
                
    class Meta:
        model = Expense
        exclude = ['created_by']

class PayForm(forms.ModelForm):
    class Meta:
        model = Pay
        exclude = ['created_by']

class DebtorForm(forms.ModelForm):
    class Meta:
        model = Debtor
        exclude = ['created_by']

class DebtForm(forms.ModelForm):
    class Meta:
        model = Debt
        exclude = ['created_by','debtor_name','fully_paid','balance','date_cleared']

class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        exclude = ['created_by','debt']
