from .models import *
from rest_framework import serializers

class DebtorSerialiser(serializers.ModelSerializer):
    #created_by = serializers.ReadOnlyField('created_by.operator.username')
    
    class Meta:
        model = Debtor
        fields = ('id', 'created_by','debtor_name')

class LocationSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Location
        fields = ('id', 'location_name','location_address')

class OperatorSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Operator
        fields = ('id', 'operator','operator_address')

class PaySerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Pay
        fields = ('id', 'created_by','pay','pay_rate')

class PeriodSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Period
        fields = ('id', 'created_by','period','period_hours','personal_protection','period_pay','date_received','paid','period_week_ending_date')

class ShiftSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Shift
        fields = ('id', 'created_by','shift_period','shift_from_date_and_time','shift_to_date_and_time','shift_location','shift_hours','shift_pay_rate','shift_pay','shift_comments')

class DebtSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Debt
        fields = ('id', 'created_by','debtor_name','description','loan_amount','date_loaned','balance','date_cleared','fully_paid')

class PaymentSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Payment
        fields = ('id', 'created_by','debt','date','amount')

class ExpenseSerialiser(serializers.ModelSerializer):
     
    class Meta:
        model = Expense
        fields = ('id', 'created_by','date_bought','item_description','item_price','item_quantity')