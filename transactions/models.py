from datetime import datetime, timedelta, date
from django.utils import timezone
from django.db import models
from decimal import *
from import_export import resources
from django.db.models import Sum
from django.contrib.auth.models import User


# Create your models here.
class Location (models.Model):
     location_name = models.CharField(max_length=200)
     location_address = models.CharField(max_length=200)

     def __unicode__(self):
          return self.location_name

#class Operator (models.Model):
#     operator = models.CharField(max_length=200)

#     def __unicode__(self):
#          return self.operator_name

class Operator (models.Model):
     operator = models.OneToOneField(User)
     operator_address = models.CharField(max_length=200,blank=True, null=True)

     def __unicode__(self):
          return self.operator.username

class Pay (models.Model):
     created_by = models.ForeignKey(Operator)
     pay = models.CharField(max_length=200)
     pay_rate = models.DecimalField(max_digits=9, decimal_places=2, default=9)

     def __unicode__(self):
          #return '%s - %s' % (self.pay, self.pay_rate)
          return '%s' % (self.pay_rate)

class Period (models.Model):
     created_by = models.ForeignKey(Operator)
     period = models.CharField(max_length=30)
     period_hours = models.DecimalField(max_digits=9, decimal_places=2,default=1)
     personal_protection = models.DecimalField(max_digits=9, decimal_places=2,default=1 )
     period_pay = models.DecimalField(max_digits=9, decimal_places=2,default=1)
     date_received =  models.DateField(auto_now = False)
     paid = models.BooleanField(default=False)
     period_week_ending_date = models.DateField(auto_now = False, blank = True, null = True)
     
     def shifts_this_period(self):
       return Shift.objects.filter(shift_period = self).count()

     def period_total_hours(self):
       shifts = Shift.objects.filter(shift_period = self)
       total_hours = sum([x.shift_hours for x in shifts])
       return total_hours

     def period_total_pay(self):
       shifts = Shift.objects.filter(shift_period = self)
       total_pay = sum([x.shift_pay for x in shifts])
       return total_pay
     
     def save(self, *args, **kwargs):
       self.period_hours = self.period_total_hours()
       self.personal_protection = 5 if self.shifts_this_period() >= 2 else 2.5
       self.period_pay = float(self.period_total_pay()) - float(self.personal_protection)
       self.date_received = datetime.now() if self.paid else '1999-01-01'
       self.period_week_ending_date = self.week_ending_date()
       return super(Period, self).save(*args, **kwargs)
     
     def week_ending_date(self):
        todays_date = date.today()
        
        if todays_date.weekday() != 6:
          sundays_date = todays_date + timedelta(days=6-todays_date.weekday())
        else:
          sundays_date = todays_date
        
        return sundays_date

     def __unicode__(self):
        return self.period
     
     class Meta:
          ordering = ["-id"]

class Shift (models.Model):
     created_by = models.ForeignKey(Operator)
     shift_period = models.ForeignKey(Period)
     shift_from_date_and_time = models.DateTimeField('From')
     shift_to_date_and_time = models.DateTimeField('To')
     shift_location = models.ForeignKey(Location)
     shift_hours = models.DecimalField(max_digits=9, decimal_places=2,default=1)
     shift_pay_rate = models.ForeignKey(Pay)
     shift_pay = models.DecimalField(max_digits=9, decimal_places=2)
     shift_comments = models.CharField(max_length=1000, default = 'N/a')

     def hours_worked_string(self):
          seconds = (self.shift_to_date_and_time - self.shift_from_date_and_time).seconds
          hours = (seconds/60/60)
          minutes = (seconds//60)%60
          return  '%shrs %smins' % (hours, minutes)
   
     def hours_worked(self):
          seconds = (self.shift_to_date_and_time - self.shift_from_date_and_time).seconds
          hours = (seconds/60.0/60.0)
          return hours

     def save(self, *args, **kwargs):
          self.shift_hours = self.hours_worked()
          self.shift_pay = self.hours_worked() * float(self.shift_pay_rate.pay_rate)
          self.shift_period.save()
          return super(Shift, self).save(*args, **kwargs)

     def __unicode__(self):
          from_time = self.shift_from_date_and_time.strftime("%a %d %B %Y")
          return unicode('%s %s' % (self.shift_location,from_time))
     
     class Meta:
          ordering = ["-id"]

class Debtor (models.Model):
     created_by = models.ForeignKey(Operator)
     debtor_name = models.CharField(max_length=30)

     def __unicode__(self):
         return self.debtor_name

class Debt (models.Model):
     created_by = models.ForeignKey(Operator)
     debtor_name = models.ForeignKey(Debtor)
     description = models.CharField(max_length=200)
     loan_amount = models.DecimalField(max_digits=9, decimal_places=2)
     date_loaned =  models.DateField(auto_now = False)
     balance = models.DecimalField(max_digits=9, decimal_places=2, null = True, blank = True)
     date_cleared =  models.DateField(null = True, blank = True)
     fully_paid = models.BooleanField(default=False)
     
     def loan_balance(self):
      payments = self.Payments.filter(debt = self)
      list_of_payments = list(payments.aggregate(Sum('amount')).values())
      payments_so_far = list_of_payments[0] if list_of_payments[0] is not None else 0
      bal = self.loan_amount - payments_so_far
      return bal
    
     def set_fully_paid(self):
        fully_paid = True if self.balance <= 0 else False
        return fully_paid
    
     def set_date_cleared(self):
        if self.fully_paid:
          date_cleared = date.today()
        else:
          date_cleared = None
          
        return date_cleared
      
     def save(self, *args, **kwargs):
          self.balance = self.loan_balance()
          self.fully_paid = self.set_fully_paid()
          self.date_cleared = self.set_date_cleared()
          return super(Debt, self).save(*args, **kwargs)

     def __unicode__(self):
         return  '%s %s %s' % (self.debtor_name, self.description, self.balance)

class Payment (models.Model):
     created_by = models.ForeignKey(Operator)
     debt = models.ForeignKey(Debt, related_name = 'Payments')
     date =  models.DateField(auto_now = False)
     amount = models.DecimalField(max_digits=9, decimal_places=2)
     
  
     def save(self, *args, **kwargs):
          self.debt.save()
          return super(Payment, self).save(*args, **kwargs)

     def __unicode__(self):
         return '%s - %s' % (self.debt, self.amount)

class Expense (models.Model):
     created_by = models.ForeignKey(Operator)
     date_bought = models.DateTimeField('Date Bought')
     item_description = models.CharField(max_length=200)
     item_price = models.DecimalField(max_digits=9, decimal_places=2)
     item_quantity = models.IntegerField(default=1)
     
     def total_cost(self):
          return self.item_price * self.item_quantity

     def was_bought_recently(self):
          return self.date_bought >= timezone.now() - datetime.timedelta(days=7)

     def __unicode__(self):
          return self.item_description
     
     class Meta:
          ordering = ["-date_bought","-id"]
     
class PayResource (resources.ModelResource):
     
     class Meta:
          model = Pay

class LocationResource (resources.ModelResource):
     
     class Meta:
          model = Location

class OperatorResource (resources.ModelResource):
     
     class Meta:
          model = Operator

class PeriodResource (resources.ModelResource):
     
     class Meta:
          model = Period

class ShiftResource (resources.ModelResource):
     
     class Meta:
          model = Shift

class ExpenseResource (resources.ModelResource):
     
     class Meta:
          model = Expense

class DebtorResource (resources.ModelResource):
     
     class Meta:
          model = Debtor

class DebtResource (resources.ModelResource):
     
     class Meta:
          model = Debt

class PaymentResource (resources.ModelResource):
     
     class Meta:
          model = Payment
