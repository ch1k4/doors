from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from transactions import RESTapi

urlpatterns = patterns('',
    #debtor
    url(r'^debtor/$', RESTapi.DebtorList.as_view()),
    url(r'^debtor/(?P<pk>\d+)/$', RESTapi.DebtorDetail.as_view()),
    #location
    url(r'^location/$', RESTapi.LocationList.as_view()),
    url(r'^location/(?P<pk>\d+)/$', RESTapi.LocationDetail.as_view()),
    #operator
    url(r'^operator/$', RESTapi.OperatorList.as_view()),
    url(r'^operator/(?P<pk>\d+)/$', RESTapi.OperatorDetail.as_view()),
    #pay
    url(r'^pay/$', RESTapi.PayList.as_view()),
    url(r'^pay/(?P<pk>\d+)/$', RESTapi.PayDetail.as_view()),
    #period
    url(r'^period/$', RESTapi.PeriodList.as_view()),
    url(r'^period/(?P<pk>\d+)/$', RESTapi.PeriodDetail.as_view()),
    #shift
    url(r'^shift/$', RESTapi.ShiftList.as_view()),
    url(r'^shift/(?P<pk>\d+)/$', RESTapi.ShiftDetail.as_view()),
    #debt
    url(r'^debt/$', RESTapi.DebtList.as_view()),
    url(r'^debt/(?P<pk>\d+)/$', RESTapi.DebtDetail.as_view()),
    #payment
    url(r'^payment/$', RESTapi.PaymentList.as_view()),
    url(r'^payment/(?P<pk>\d+)/$', RESTapi.PaymentDetail.as_view()),
    #expense
    url(r'^expense/$', RESTapi.ExpenseList.as_view()),
    url(r'^expense/(?P<pk>\d+)/$', RESTapi.ExpenseDetail.as_view()),
)

urlpatterns = format_suffix_patterns(urlpatterns)