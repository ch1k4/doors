from django.conf.urls import patterns, url
from transactions.views import *

urlpatterns = patterns('',
    #Shifts
    url(r'^shift/list/$', ShiftList.as_view(), name="shift-list"),
    url(r'^shift/details/(?P<pk>\d+)/$', ShiftDetail.as_view(), name='shift-detail'),
    url(r'^shift/create/$', CreateShift.as_view(), name='shift-new'),
    url(r'^shift/update/(?P<pk>\d+)/$', UpdateShift.as_view(), name='shift-update'),
    
    #Debtors 	
    url(r'^debtor/list/$', DebtorList.as_view(), name='debtor-list'),
    url(r'^debtor/debts/(?P<pk>\d+)/$', DebtorDebt.as_view(), name='debtor-debt'),
    url(r'^debtor/create/$', CreateDebtor.as_view(), name='debtor-new'),
    url(r'^debtor/update/(?P<pk>\d+)/$', UpdateDebtor.as_view(), name='debtor-update'),
    
    #Debts
    url(r'^debt/update/(?P<pk>\d+)/$', UpdateDebt.as_view(), name='debt-update'),
    url(r'^debt/details/(?P<pk>\d+)/$', DebtDetail.as_view(), name='debt-detail'),
    url(r'^debt/create/(?P<pk>\d+)/$', CreateDebt.as_view(), name='debt-new'),
    
    #Expenses 	
    url(r'^expense/list/$', ExpenseList.as_view(), name='expense-list'),
    url(r'^expense/create/$', CreateExpense.as_view(), name='expense-new'),
    url(r'^expense/update/(?P<pk>\d+)/$', UpdateExpense.as_view(), name='expense-update'),
    
    #Locations 	
    url(r'^location/list/$', LocationList.as_view(), name='location-list'),
    url(r'^location/create/$', CreateLocation.as_view(), name='location-new'),
    url(r'^location/update/(?P<pk>\d+)/$', UpdateLocation.as_view(), name='location-update'),
    
    #Operators 	
    url(r'^personnel/list/$', OperatorList.as_view(), name='personnel-list'),
    url(r'^personnel/create/$', CreatePersonnel.as_view(), name='personnel-new'),
    url(r'^personnel/update/(?P<pk>\d+)/$', UpdatePersonnel.as_view(), name='personnel-update'),
    
    #Payments 	
    url(r'^payment/update/(?P<pk>\d+)/$', UpdatePayment.as_view(), name='payment-update'),
    url(r'^payment/create/(?P<pk>\d+)/$', CreatePayment.as_view(), name='payment-new'),
    
    #Pays 
    url(r'^pay/list/$', PayList.as_view(), name='pay-list'),	
    url(r'^pay/create/$', CreatePay.as_view(), name='pay-new'),
    url(r'^pay/update/(?P<pk>\d+)/$', UpdatePay.as_view(), name='pay-update'),
    
    #Periods 	
    url(r'^period/list/$', PeriodList.as_view(), name="period-list"),
    url(r'^period/shifts/(?P<periodpk>\d+)/$', PeriodShifts.as_view(), name='period-shifts'),
    url(r'^period/details/(?P<pk>\d+)/$', PeriodDetail.as_view(), name='period-detail'),
    url(r'^period/create/$', CreatePeriod.as_view(), name='period-new'),
    url(r'^period/update/(?P<pk>\d+)/$', UpdatePeriod.as_view(), name='period-update'),
    
    #Emails
    url(r'^email/form/(?P<pk>\d+)/$', ContactFormView.as_view(), name='contact-form'),
    url(r'^email/form/(?P<pk>\d+)/sent/$', EmailSent.as_view(), name='email-sent'),
)