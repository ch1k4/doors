from django.test import TestCase
from transactions.models import *

class LocationTests(TestCase):
    """ Location test """
    
    def test_unicode(self):
        location = Location(
            location_name = "Bankers",
            location_address = "Town"
        )
        
        self.assertEquals(unicode(location), 'Bankers',)

