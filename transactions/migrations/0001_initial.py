# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Expense'
        db.create_table(u'transactions_expense', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date_bought', self.gf('django.db.models.fields.DateTimeField')()),
            ('item_description', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('item_price', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('item_quantity', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'transactions', ['Expense'])

        # Adding model 'Location'
        db.create_table(u'transactions_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('location_address', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'transactions', ['Location'])

        # Adding model 'Shift'
        db.create_table(u'transactions_shift', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shift_from_date_and_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('shift_to_date_and_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('shift_location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['transactions.Location'])),
            ('shift_hours', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
        ))
        db.send_create_signal(u'transactions', ['Shift'])

        # Adding model 'Pay'
        db.create_table(u'transactions_pay', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shift', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['transactions.Shift'])),
            ('rate', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('actual_amount', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('expected_amount', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('personal_protection', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('date_received', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'transactions', ['Pay'])

        # Adding model 'Debtor'
        db.create_table(u'transactions_debtor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('debtor_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('debt_reason', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('loan_amount', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('date_loaned', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('date_received', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('amount_received', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=2)),
            ('fully_paid', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('part_paid', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'transactions', ['Debtor'])


    def backwards(self, orm):
        # Deleting model 'Expense'
        db.delete_table(u'transactions_expense')

        # Deleting model 'Location'
        db.delete_table(u'transactions_location')

        # Deleting model 'Shift'
        db.delete_table(u'transactions_shift')

        # Deleting model 'Pay'
        db.delete_table(u'transactions_pay')

        # Deleting model 'Debtor'
        db.delete_table(u'transactions_debtor')


    models = {
        u'transactions.debtor': {
            'Meta': {'object_name': 'Debtor'},
            'amount_received': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'date_loaned': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'date_received': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'debt_reason': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'debtor_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'fully_paid': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loan_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'part_paid': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'transactions.expense': {
            'Meta': {'object_name': 'Expense'},
            'date_bought': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item_description': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'item_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'item_quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'transactions.location': {
            'Meta': {'object_name': 'Location'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location_address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'location_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'transactions.pay': {
            'Meta': {'object_name': 'Pay'},
            'actual_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'date_received': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'expected_amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personal_protection': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'shift': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transactions.Shift']"})
        },
        u'transactions.shift': {
            'Meta': {'object_name': 'Shift'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shift_from_date_and_time': ('django.db.models.fields.DateTimeField', [], {}),
            'shift_hours': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '2'}),
            'shift_location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['transactions.Location']"}),
            'shift_to_date_and_time': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['transactions']